# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://rokcej@bitbucket.org/rokcej/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/rokcej/stroboskop/commits/7b65a86b22ad973df91b59ffed4f66c9f9e397f4

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/rokcej/stroboskop/commits/8873886138797e36f9fca5cb2925622f03cb8cb2

Naloga 6.3.2:
https://bitbucket.org/rokcej/stroboskop/commits/2cfcf310070e3e5c2030ed525488040d34b0991b

Naloga 6.3.3:
https://bitbucket.org/rokcej/stroboskop/commits/ced9c39e02b12215b67fd6366c4851d43f0d292b

Naloga 6.3.4:
https://bitbucket.org/rokcej/stroboskop/commits/fa1ae73552fab26747f1a40c0d814060b15fbe2f

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/rokcej/stroboskop/commits/0e08aabcc68518bd0b53456caf55b613f38b7160

Naloga 6.4.2:
https://bitbucket.org/rokcej/stroboskop/commits/42e2ee196c75b81ade9f42234f058953400bea15

Naloga 6.4.3:
https://bitbucket.org/rokcej/stroboskop/commits/5fbed8a6e8d282fc89ebaf5694334dc053b69c3b

Naloga 6.4.4:
https://bitbucket.org/rokcej/stroboskop/commits/1a22456ea7dbb7c538701dab18c2502eca469f66